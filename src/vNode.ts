import { any, fromType, VueTypeValidableDef } from 'vue-types'
import type { VNodeChild, VNode } from 'vue'

const vNode = () => fromType('vNode', any() as VueTypeValidableDef<VNodeChild | VNode>)

export default vNode
