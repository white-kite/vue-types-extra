import { fromType, object, toValidableType } from 'vue-types'
import type { Properties } from 'csstype'

export type CSSProperties = Properties<number | string>
const styleObject = () => fromType('styleObject', object<CSSProperties>())
const style = () => toValidableType<string | CSSProperties>('style', { type: [String, Object] })
export { style, styleObject }
