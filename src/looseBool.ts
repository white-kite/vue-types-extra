import { toType } from 'vue-types'
const looseBool = () =>
  toType('looseBool', {
    type: Boolean,
  })
export default looseBool
