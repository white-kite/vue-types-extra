# whitekite-vue-types-extra

vue-types的额外属性

[![star](https://gitee.com/white-kite/vue-types-extra/badge/star.svg?theme=dark)](https://gitee.com/white-kite/vue-types-extra/stargazers)
[![fork](https://gitee.com/white-kite/vue-types-extra/badge/fork.svg?theme=dark)](https://gitee.com/white-kite/vue-types-extra/members)

基础属性见[vue-types](https://dwightjack.github.io/vue-types/)
### 特殊方法

```tsx

// 基础的bool值，不验证
looseBool().def(false)

// 说明这是一个 CSSProperties 样式对象或字符串，类型提示用
style().def(null)

// 说明这是一个 CSSProperties 样式对象，类型提示用
styleObject()

// 说明这是一个jsx组件插槽，typescript类型验证用，本质是any()的ts封装
vNode()
```
